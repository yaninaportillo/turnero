from django.apps import AppConfig
from django_seed import Seed

class AppNameConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app'

    """def ready(self):
        from app.fixtures.seeds import seeds
        seeds()"""