from django.db import models

# Create your models here.
from django.db import models


class Cliente(models.Model):
    cedula = models.CharField(max_length=20, unique=True)
    nombre = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    telefono = models.CharField(max_length=20)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_nacimiento = models.DateField(null=False, blank=False)  
        
class Prioridad(models.Model):
    descripcion = models.CharField(max_length=200, null = True)
    abreviacion = models.CharField(max_length=1, unique=True, null = True)
    cantidad_esperando = models.IntegerField(default=0)

    def __str__(self):
        return self.descripcion
    
class Servicio(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()

    def __str__(self):
        return self.nombre

class Ticket(models.Model):
    codigo = models.CharField(max_length= 4, unique = True, null= True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    servicio_elegido = models.ForeignKey(Servicio, on_delete=models.PROTECT, null=True)
    tiempo_espera = models.DurationField(null=True, blank=True)
    atendido = models.BooleanField(default=False)

    def __str__(self):
        return self.codigo
    
class Cola(models.Model):
    prioridad = models.ForeignKey(Prioridad, on_delete=models.PROTECT, null=True)
    ticket = models.ForeignKey(Ticket, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.ticket.codigo




