from django import forms
from .models import  Prioridad

class InicioForm(forms.Form):
    cedula = forms.CharField(label='Cédula', max_length=10)


class DateInput(forms.DateInput):
    input_type = 'date'
    format='%d/%m/%Y'


class RegistroForm(forms.Form):
    cedula = forms.CharField(widget=forms.HiddenInput())
    nombre = forms.CharField(max_length=50)
    apellidos = forms.CharField(max_length=50)
    fecha_nacimiento = forms.DateField(widget=DateInput())
    telefono = forms.CharField(max_length=20)


class PrioridadForm(forms.Form):
    prioridad = forms.ModelMultipleChoiceField(label="Prioridades", queryset=Prioridad.objects.all(), widget=forms.CheckboxSelectMultiple(), required=False)