from app.models import Prioridad

def seeds():
    # Crear instancias de Prioridad
    prioridades = [
        {"descripcion": "Prioridad Alta para embarazadas, personas de tercera edad y personas con alguna discapacidad", "abreviacion": "A"},
        {"descripcion": "Prioridad Media para clientes corporativos", "abreviacion": "B"},
        {"descripcion": "Prioridad Baja para clientes personales", "abreviacion": "C"},
    ]
    for prioridad in prioridades:
        Prioridad.objects.create(**prioridad)