from django.shortcuts import render,redirect, get_object_or_404
from django.views import View
from django.db.models import Min
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from .forms import InicioForm, RegistroForm, PrioridadForm
from .models import Cliente, Prioridad, Ticket, Cola, Servicio
from datetime import date
import datetime


# Create your views here.


def inicio(request):
    if request.method == 'POST':
        form = InicioForm(request.POST)
        if form.is_valid():
            cedula = form.cleaned_data['cedula']
            cliente = Cliente.objects.filter(cedula=cedula).first()
            if cliente:
                return redirect('prioridad', cliente_id=cliente.pk)
            else:
                return redirect('registro', cedula=cedula)
    else:
        form = InicioForm()
    return render(request, 'inicio.html', {'form': form})


def registro(request, cedula):
    if request.method == 'POST':
        form = RegistroForm(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            apellidos = form.cleaned_data['apellidos']
            fecha_nacimiento = form.cleaned_data['fecha_nacimiento']
            fecha_nacimiento = datetime.datetime.strptime(str(fecha_nacimiento), '%Y-%m-%d').date() # formatear la fecha ingresada
            telefono = form.cleaned_data['telefono'] 
            cliente = Cliente(cedula=cedula, nombre=nombre, apellidos=apellidos, fecha_nacimiento=fecha_nacimiento, telefono=telefono)
            cliente.save()
            return redirect('prioridad', cliente_id=cliente.pk)
    else:
        form = RegistroForm(initial={'cedula': cedula})
    return render(request, 'registro.html', {'form': form, 'cedula': cedula})


def prioridad(request, cliente_id):
    cliente = Cliente.objects.get(id=cliente_id)
    prioridades = Prioridad.objects.all()
    if request.method == 'POST':
        form = PrioridadForm(request.POST)
        if form.is_valid():
            selected_priorities = request.POST.getlist('prioridad[]')
            if selected_priorities:
                abreviacion_prioridad = selected_priorities[0]
                prioridad = Prioridad.objects.get(abreviacion=abreviacion_prioridad)
            else:
                today = date.today()
                age = today.year - cliente.fecha_nacimiento.year - ((today.month, today.day) < (cliente.fecha_nacimiento.month, cliente.fecha_nacimiento.day))
                if age >= 60:
                    prioridad = Prioridad.objects.get(abreviacion='A')
                else:
                    prioridad = Prioridad.objects.get(abreviacion='C')
            prioridad.cantidad_esperando += 1
            prioridad.save()
            codigo = f'{prioridad.abreviacion}{prioridad.cantidad_esperando:03}'
            ticket = Ticket(cliente=cliente, servicio_elegido = None, codigo=codigo)
            ticket.save()
            return redirect('servicio', ticket_id=ticket.pk, prioridad_id = prioridad.pk)
    else:
        form = PrioridadForm()
    return render(request, 'prioridad.html', {'form': form, 'cliente': cliente, 'prioridades': prioridades})

def servicio(request, ticket_id, prioridad_id):
    ticket = Ticket.objects.get(id=ticket_id)
    servicios = Servicio.objects.all()
    if request.method == 'POST':
        selected_service = request.POST.get('servicio')
        if selected_service:
            servicio = Servicio.objects.get(id=selected_service)
            ticket.servicio_elegido = servicio
            ticket.save()
            return redirect('detalle', ticket_id=ticket.pk)
    prioridad = Prioridad.objects.get(pk=prioridad_id)
    nuevo_ticket_en_cola = Cola(prioridad=prioridad, ticket=ticket)
    nuevo_ticket_en_cola.save()
    return render(request, 'servicio.html', {'servicios': servicios,'ticket_id': ticket_id, 'prioridad_id':prioridad_id})


def detalle(request, ticket_id):
    ticket = get_object_or_404(Ticket, id=ticket_id)
    return render(request, 'detalle.html', {'ticket': ticket})

class ColaView(View):
    template_name = 'cola.html'

    def get(self, request):
        tickets_en_cola = Cola.objects.annotate(
            min_fecha_creacion=Min('ticket__fecha_creacion')
        ).order_by('prioridad', 'min_fecha_creacion')
        context = {
        'tickets_en_cola': tickets_en_cola,
        }
        return render(request, self.template_name, context)
    
def llamar_ticket(request):
    if request.method == 'POST':
        codigo = request.POST.get('ticket_codigo')
        try:
            ticket = Ticket.objects.get(codigo=codigo, atendido=False)
            ticket.atendido = True
            ticket.save()
            # Eliminar el ticket de la cola
            cola = Cola.objects.get(ticket_id=ticket)
            cola.delete()
            messages.success(request, f'Se llamó al ticket {ticket.codigo} para el servicio {ticket.servicio_elegido.descripcion}')
        except Ticket.DoesNotExist:
            messages.error(request, f'El ticket {codigo} no existe o ya ha sido atendido.')
        return redirect(reverse('cola'))

    return HttpResponseRedirect(reverse('cola'))
