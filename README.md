Turnero
Descripción
Turnero es una aplicación web para la gestión de la cola de clientes en una empresa. Permite registrar a los clientes, agendar su atención según su nivel de prioridad y visualizar la cola de atención.

Requisitos previos
Python 3.x

Configuración del entorno virtual
Abre una terminal y navega hasta la carpeta del proyecto "turnero".
Crea y activa un entorno virtual con el siguiente comando:
>python3 -m venv turnero_env
>./turnero_env/Scripts/activate

Para instalar todas las dependencias utiliza el comando:
>python -m pip install -r requirements.txt

Ejecuta el servidor de desarrollo de Django con el siguiente comando:
python manage.py runserver

Abre tu navegador web y accede a http://localhost:8000 para ver la aplicación.
